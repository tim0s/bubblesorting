package ru.bubblesorting;

import java.util.Arrays;

public class BubbleSorting {
    // Array to be sorting
    private final static int[] array = {5, 4, 1, 54, 34};

    //Swap elements in an array
    private void swapElements(int[] array, int firstIndex, int secondIndex) {
        int tempVariable = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = tempVariable;
    }

    //Sorting algorithm
    public void bubbleSorting() {
        System.out.println(Arrays.toString(array));
        boolean isNeedIteration = true;
        while (isNeedIteration) {
            isNeedIteration = false;
            for (int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    swapElements(array, i, i - 1);
                    isNeedIteration = true;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
